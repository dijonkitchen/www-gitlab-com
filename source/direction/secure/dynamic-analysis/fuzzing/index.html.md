---
layout: markdown_page
title: "Category Direction - Fuzzing"
---

- TOC
{:toc}

## Overview
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Fuzz testing, or fuzzing, is the act of inputting unexpected, malformed and/or random data to measure response or stability of an application or service.  This is accomplished by monitoring for unexpected behavior or service / application crashes.  The goal of fuzzing is to discover software defects that need to be addressed as these defects may lead to (exploitable) vulnerabilities.

Fuzz testing can be performed using one of the two [fuzz algorithm strategies](https://resources.infosecinstitute.com/fuzzing-mutation-vs-generation/):
* Mutation → Also known as dumb fuzzing.  In this algorithm strategy, the fuzz testing solution knows nothing about the structure of the input it is creating.  As such, mutation fuzzing solutions create tests by doing things such as bit flipping and appending additional data.
* Generation → Also known as intelligent fuzzing.  In this algorithm strategy, the fuzz testing solution understands the input of the application or service it is fuzzing.  As such, generation fuzzing solutions create tests following the state model as well as the input structure allowing for deeper code coverage.

Fuzz testing can also be applied two different ways:
* [Black-box](https://en.wikipedia.org/wiki/Black-box_testing) → This type of fuzz testing verifies the application or service while it is running.  As such, this type of fuzz testing does not have visibility into the internal components of the application or service and focuses on input entered into the service or application as well as the output generated from said input.
* [White-box](https://en.wikipedia.org/wiki/White-box_testing) → This type of fuzz testing verifies the internal application or service architecture and is usually performed with the application or service at rest (i.e., not running).  As such, it does not have visibility into how the application or service operates when it is online.

## Target audience and experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

There are two primary target audiences:
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst) → Roles like Sam look for vulnerabilities or security flaws within the solution they are testing.  Furthermore, organizations with security operations center (SOC) engineers or internal red teams verify new software they internally create or verify solutions they purchase, either software or network security platforms, to confirm they are safe to put onto their production networks.
* [Sasha (Software Developer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) → Roles like Sasha want to minimize the software defects in the solutions they develop while creating stable and reliable solutions.  Software development teams at organizations that develop solutions such as web application firewalls (WAF) or next-generation firewalls (NGFW) need to verify their solutions are ready for their customers.

There is an additional role which may be included in the target audience.  Product verification (PV) engineers may leverage fuzz testing as part of their unit or regression tests.  If the software development team does not directly handle their unit test creation, a PV engineer may be partnered with a software development team to create and maintain unit tests on behalf of the software development team.

## Challenges to address

Each target audience has different challenges that need to be addressed with our fuzz testing solution:
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst) → Roles like Sam need to be able to hunt for zero-day vulnerabilities as well as other software defects that could damage the company in which they are employed (e.g., reputation, financially).  These roles need to be able to perform tests quickly, efficiently, and without the burden of becoming an expert in the tool they are using.
* [Sasha (Software Developer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) → Roles like Sasha only want to think about the valid input/response and have an expectation that invalid input is handled gracefully (e.g., doesn’t crash or return data it shouldn’t).  These roles need testing solutions to be easily accessible and included into their build toolchain.

## Where we are Headed

GitLab will provide its users with application robustness and security visibility testing solutions which validate the entire application stack.  This will be provided by verifying the user’s application or service both while at rest (white-box testing) as well as while running (black-box testing).  This will include historical trending and recommendations for next steps to provide peace of mind to our customers.

Furthermore, GitLab will provide real-time remediation for user solutions running in production.  This will be possible by analyzing the user issues found in GitLab Secure and apply “[virtual patches](https://www.owasp.org/index.php/Virtual_Patching_Best_Practices)” to the user’s production application or service leveraging GitLab Defend. This will allow your organization to be secure and continue functioning until the underlying vulnerability can be remediated in your code.

### What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

The next step is to provide a minimal support for fuzzing as part of GitLab DAST offering. Users can set up dedicated pipelines and run fuzzing sessions. This is the very minimal goal to dig into this category.

- [MVC](https://gitlab.com/gitlab-org/gitlab/issues/33906)

### What is Not Planned Right Now
Wireless, Bluetooth, and Automotive based fuzzing as these types of fuzz testing solutions require special hardware to accomplish.  This makes these types of fuzz testing out of scope.

Additionally, our initial focus is on [network applications and services](https://about.gitlab.com/direction/maturity/#application-type-maturity), so we are not focusing on fuzzing local desktop or mobile applications at this time.

### Maturity Plan

 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/818)
 
## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The fuzz testing competitive landscape is composed of both commercial and open source testing solutions.  The following outlines the competitive landscape for fuzz testing solutions:
* Commercial
  - [Synopsys / Codenomicon Defensics](https://www.synopsys.com/software-integrity/security-testing/fuzz-testing.html)
  - [PeachTech Peach Fuzzer Platform](https://www.peach.tech/products/peach-fuzzer/)
  - [Spirent CyberFlood Advanced Fuzzing](https://www.spirent.com/products/cyberflood)
  - [Beyond Security BeStorm](https://www.beyondsecurity.com/bestorm.html)
* Open source
  - [Sulley framework](https://github.com/OpenRCE/sulley)
  - [American fuzzy lop](http://lcamtuf.coredump.cx/afl/)
  - [boofuzz](https://github.com/jtpereyda/boofuzz)
  - [PeachTech Peach Fuzzer Community Edition](https://www.peach.tech/resources/peachcommunity/)
  - [OWASP Zed Attack Proxy (ZAP)](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project)

GitLab already uses OWASP [ZAP](https://github.com/zaproxy/zap-core-help) today to power the DAST capability within the GitLab Secure offerings.  ZAP is a web application security scanner, leveraging a proxy to perform testing, and does not perform protocol-level fuzzing.  All fuzzing by ZAP is performed within the context of the web application being tested.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

Analysts consider fuzzing to be part of Application Security Testing and is generally discussed as part of those reports, rather than as a standalone capability.
There are challenges to make it part of the DevSecOps paradigm, and they are interested to hear more about that.

By combining our fuzz testing solution with our DAST, IAST, and SAST solutions into a GitLab Secure suite, GitLab can be placed into the Gartner [Magic Quadrant for Application Security Testing](https://www.gartner.com/en/documents/3906990/magic-quadrant-for-application-security-testing). This will give GitLab additional exposure as well as show security thought leadership.

Other analyst firms include 451 Research and IDC as they have focused security practices in which GitLab Secure can be highlighted and show leadership.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [MVC](https://gitlab.com/gitlab-org/gitlab/issues/33906)
